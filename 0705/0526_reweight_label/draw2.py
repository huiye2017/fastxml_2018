import numpy as np
from dataStructure import *
import matplotlib.pyplot as plt

file_dir = './result/' + 'Bibtex' + '/model/'
file_name = file_dir + 'weight_'+ str(1)
        # weight_file = open(weight_name, 'r')
file = open(file_name, 'r')
lines = file.readlines()
value_list = []
for line in lines:
    value = float(line.strip())
    value_list.append(value)
label_weights = np.array(value_list)

data_set_name = 'Bibtex'
train_feature_matrix = SparseMatrix('/home/huy217/fastXML/dataset/' + data_set_name + '/' + data_set_name + '_train_feature.txt').matrix  # read the feature data
train_label_matrix = SparseMatrix('/home/huy217/fastXML/dataset/' + data_set_name + '/' + data_set_name + '_train_label.txt').matrix  # read the label data
test_feature_matrix = SparseMatrix('/home/huy217/fastXML/dataset/' + data_set_name + '/' + data_set_name + '_test_feature.txt').matrix
truth_label_matrix = SparseMatrix('/home/huy217/fastXML/dataset/' + data_set_name + '/' + data_set_name + '_test_label.txt').matrix

num_label_array = numpy.array(train_label_matrix.sum(axis = 1)).flatten()
label_index = np.argsort(num_label_array)
y = label_weights[label_index]
x = np.log(num_label_array[label_index])
plt.plot(x,y,'r',linewidth=2)
plt.show()