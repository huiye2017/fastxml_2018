import numpy as np
import matplotlib.pyplot as plt

file_name = './' + 'result/Bibtex/eval_result_infrequent.txt'
file = open(file_name, 'r')
lines = file.readlines()
file.close()
value_list = []
temp_list = []
for line in lines:
    pairs = line.strip().split(":")
    if pairs[0].strip() == 'P1':
        temp_list.append(float(pairs[1].strip()))
    else:
        if pairs[0].strip() == 'P3':
            temp_list.append(float(pairs[1].strip()))
        else:
            if pairs[0].strip() == 'P5':
                temp_list.append(float(pairs[1].strip()))
                value_list.append(temp_list)
                temp_list = []
            # value = float(line.strip())
value_array = np.array(value_list)
# print(value_array[0,:])
x=np.linspace(1,50,50)
f1=value_array[:,0].flatten()
f2=value_array[:,1].flatten()
f3=value_array[:,2].flatten()
plt.plot(x,f1,'r',x,f2,'b',x,f3,'g',linewidth=2)
# plt.axis([1,50,0,1])
# plt.text(1,7.5,r'$P1$',fontsize=16)
# plt.text(2.2,7.5,r'$P3$',fontsize=16)
# plt.text(3.2,7.5,r'$P5$',fontsize=16)
# plt.title('A simple example',fontsize=16)
plt.show()