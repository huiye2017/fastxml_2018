import  pickle
import numpy
import sys
from scipy.sparse import csc_matrix
import time


class SparseMatrix:
    def __init__(self, file_name = ''):
        self.num_col = 0
        self.num_row = 0
        self.num_nonzero = []
        self.matrix = csc_matrix([])
        self.data = []

        row_list = []
        col_list = []
        value_list = []

        file = open(file_name, 'r')

        # read the column and row of the matrix
        first_line = file.readline().strip().split(" ")
        self.num_col = int(first_line[0])
        self.num_row = int(first_line[1])

        # read the non-zero entries of the matrix
        lines = file.readlines()
        # time_begin = time.time()
        for i in range(0,len(lines)):
            # if i > 90:
            #     print('i= ',i)
            line = lines[i]     # line = '0:1 3:1'
            pairs = line.strip().split(" ")  # pairs = ['0:1','3:1']
            # debug code
            # if i > 50:
            #     print(file_name, i, 'len(pairs)', len(pairs), pairs)
            # pairs = ['0:1','3:1']
            # print(len(pairs))
            #
            # pairs = ['']
            # print(len(pairs))

            if pairs != ['']:   # if the line is not empty
                for pair in pairs:  # pair = ['0:1']
                    try:
                        pair_string = pair.split(":") # pair_string = ['0','1']
                        row_list.append(int(pair_string[0]))
                        col_list.append(i)
                        value_list.append(float(pair_string[1]))
                    except:
                        print("error!", sys.exc_info()[0], "occured.")
                        print('the', i, 'line')
                        # print('line= ',line)
                        # print('pair= ',pair)
                self.num_nonzero.append(len(pairs))
            else:
                self.num_nonzero.append(0)
        # print('the cost of for:', time.time() - time_begin)

        # time_begin = time.time()
        # for i in range(10):
        self.matrix = csc_matrix((value_list, (row_list, col_list)), (self.num_row, self.num_col))
        # print('the cost of matrix:', time.time() - time_begin)

        # test_list = []
        # time_begin = time.time()
        # for i in range(10):
        #     test_list.append(i)
        # print('the cost of list:', time.time() - time_begin)


        # for element in self.matrix.toarray():
        #     for entry in element:
        #         print(entry)
        # print(self.matrix[44,0])
        # print(self.matrix[47, 0])
        # print(self.matrix[48, 0])

        file.close()

        # self.num_col = 0
        # self.num_row = 0
        # self.num_nonzero = []
        # self.data = []
        #
        # file = open(file_name, 'r')
        #
        # #read the colunm and row of the matrix
        # first_line = file.readline().strip().split(" ")
        # self.num_col = int(first_line[0])
        # self.num_row = int(first_line[1])
        #
        # #read the indices and values of the entries of the matrix
        # lines = file.readlines()
        # for line in lines:   #line = '0:1 3:1'
        #     pairs = line.strip().split(" ") # pairs = ['0:1','3:1']
        #
        #     pair_tuple_list = []
        #     for pair in pairs:     # pair = ['0:1']
        #         pair_string = pair.split(":") # pair_string = ['0','1']
        #         #pair_dict = {int(pair_string[0]): float(pair_string[1])} # pair_dict = {0:1}
        #         pair_tuple = int(pair_string[0]), float(pair_string[1])
        #         pair_tuple_list.append(pair_tuple)  # pair_dict_list = [{0:1}]
        #         # temp_list = pair_list
        #         # print(pair_list)
        #     self.num_nonzero.append(len(pair_tuple_list))
        #     self.data.append(pair_tuple_list)


class Node:
    def __init__(self, instances = [], depth = 0, max_leaf = 0 ):
        self.is_leaf = False
        self.pos_child = -1
        self.neg_child = -1
        self.depth = depth
        self.instances = instances
        self.w = []
        self.leaf_prob = []

        if len(instances) <= max_leaf:
            self.is_leaf = True
        else:
            self.is_leaf = False

    def ReadNode(file_name = ''):
        file = open(file_name, 'rb')
        node = pickle.load(file)
        return  node

    def WriteNode(self,file_name = ''):
        file = open(file_name,'wb')
        pickle.dump(self,file)
        file.close()


class Tree:
    def __init__(self, nodes = []):
        self.nodes = nodes

    def read_tree(self, file_name = ''):
        file = open(file_name, 'rb')
        self = pickle.load(file)
        file.close()
        return self

    def write_tree(self,file_name = ''):
        file = open(file_name, 'wb')
        pickle.dump(self,file)
        file.close()
