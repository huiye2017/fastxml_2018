from math import *
import numpy as np
from dataStructure import *
import fastXML
from sklearn.linear_model import LogisticRegression

label_data = SparseMatrix('train_label.txt')
feature_data = SparseMatrix('train_feature.txt')

# indices of instances
instances = [0,1,2,3,4,5,6,7,8,9,10,11]


wt_nDCG = []  # the weight of nDCG
norm_nDCG = [] # the value of normalized DCG

num_col = label_data.num_col   # the column of the sparse matrix
num_row = label_data.num_row   # the row of the sparse matrix
num_nonzero = label_data.num_nonzero # the number of nonzero labels
# data = label_data.data
# initial partition indicators
# pos_neg = [-1,1,-1,1,-1,1,-1,1,-1,1,-1,1]
pos_neg = numpy.array(range(num_col)) % 2 * 2 - 1
print('pos_neg: ',pos_neg)


ndcg = -1     # define ndcg and initialize with -1
new_ndcg = 0  # define new_ndcg and initialize with 0

# calculate wt_nDCG and norm_nDCG
# see Eq.(4) in the paper

wt_nDCG = 1 / np.log2(2 + np.array(range(num_row)))
# print(type(wt_nDCG))

# print(wt_nDCG)

# for i in range(0,num_row):
#    wt_nDCG.append(1 / log2(1 + i + 1))

for i in range(0,num_col):
     num = num_nonzero[i]

     norm_nDCG.append(1.0 / np.sum(wt_nDCG[:num]))
     # temp = 0.0
     # for j in range(0,num):
     #     temp += wt_nDCG[j]

# for coefficient in norm_nDCG:
#      print(coefficient)

while(True):
   (pos_indices,neg_indices,pos_desc,neg_desc) = fastXML.optimize_rank(label_data,pos_neg,norm_nDCG)

   # for i in pos_indices:
   #     print(i)
   # for i in neg_indices:
   #     print(i)
   # for i in pos_desc:
   #     print(i)
   # for i in neg_desc:
   #     print(i)

   print('pos_desc',type(pos_desc))

   # calculate new_ndcg
   # new_ndcg = 0.0
   # for i in range(0, num_row):
   #    new_ndcg += wt_nDCG[i] * (pos_desc[i] + neg_desc[i])
   # new_ndcg /= num_col
   # print('new_ndcg', new_ndcg)

   new_ndcg = np.dot(wt_nDCG, np.array(pos_desc) + np.array(neg_desc)) / num_col
   # print('new_ndcg', new_ndcg)

   if new_ndcg - ndcg < 1e-6: break
   else: ndcg = new_ndcg

   pos_neg = fastXML.optimize_pos_neg(label_data,pos_indices,neg_indices,norm_nDCG)
   # for i in pos_neg:
   #     print(i)

clf_l1_LR = LogisticRegression(penalty = 'l1',tol = 1e-4)
clf_l1_LR.fit(feature_data.matrix.transpose(),pos_neg)
w_array = clf_l1_LR.coef_
print(type(w_array))

for element in w_array.flatten().tolist():
    print(element)