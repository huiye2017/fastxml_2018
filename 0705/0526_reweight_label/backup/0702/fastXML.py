from math import *
from dataStructure import *
from numpy import argsort
from sklearn.linear_model import LogisticRegression
import operator
from scipy.sparse import csc_matrix
from scipy.sparse import csr_matrix
import os

global_index_tree, global_index_node,  global_num_split= 0, 0, 0
global_array = numpy.array([])


def optimize_rank(label_spa_mat, pos_neg, norm_nDCG):
    """
    This function is to find the ideal ranking of the positive and negative partition respectively.
    See Eq.(11) in the paper.
    :param label_spa_mat: the sparse matrix of labels
    :param pos_neg: the indicator of positive and negotive partitions
    :param norm_nDCG: the value of normalized DCG
    :return pos_indices: the indices of ideal ranking of positive partition
    :return neg_indices: the indices of ideal ranking of negative partition
    :return pos_desc:    the value of ideal ranking of positive partition
    :return neg_desc:    the value of ideal ranking of negative partition
    """
    time_begin = time.time()

    (num_row, num_col) = label_spa_mat.shape

    array = label_spa_mat.multiply(norm_nDCG.reshape(1,num_col))

    pos_indi = [1 if pos_neg[i] == 1 else 0 for i in range(num_col)]
    neg_indi = [1 if pos_neg[i] == -1 else 0 for i in range(num_col)]

    pos_array = array.dot(numpy.array(pos_indi).reshape(num_col,1))
    neg_array = array.dot(numpy.array(neg_indi).reshape(num_col,1))

    pos_indices = argsort(-pos_array.flatten()).tolist()  # sort the array in descending order and get the indices
    neg_indices = argsort(-neg_array.flatten()).tolist()  # sort the array in descending order and get the indices

    pos_desc = sorted(pos_array.flatten(), reverse=True)  # sort the array in descending order
    neg_desc = sorted(neg_array.flatten(), reverse=True)  # sort the array in descending order

    pos_indices_dic = {i:pos_indices.index(i) for i in range(num_row)}
    neg_indices_dic = {i:neg_indices.index(i) for i in range(num_row)}

    # print('the time of optimize_rank:', time.time() - time_begin)

    return pos_indices_dic,neg_indices_dic,pos_desc,neg_desc  # return the array and  the indices of the array


def optimize_pos_neg(label_spa_mat, pos_indices_dic, neg_indices_dic, norm_nDCG, pos_neg):
    """
    This function is to split the instances into positive and negative partitions.
    See Eq.(13) in the paper.
    :param label_spa_mat: the sparse matrix of labels
    :param pos_indices_dic: the indices of ideal ranking of positive partition
    :param neg_indices_dic: the indices of ideal ranking of negative partition
    :param norm_nDCG:   the value of normalized DCG
    :return  pos_neg:   the latest indicator of positive and negotive partitions
    """
    time_begin = time.time()

    (num_row, num_col) = label_spa_mat.shape

    diff_coeff = numpy.array([(1 / log2(1 + pos_indices_dic[i] + 1) - 1 / log2(1 + neg_indices_dic[i] + 1)) for i in range(num_row)])

    diff_array = label_spa_mat.transpose().dot(diff_coeff.reshape(num_row, 1)) * norm_nDCG.reshape(num_col,1)

    size = diff_array.size
    # pos_neg= [1 if entry > 0 else -1 for entry in diff_array.flat]
    # pos_neg = [0] * num_col
    for i in range(diff_array.size):
        if diff_array.flat[i] > 0: pos_neg[i] = 1
        else:
            if diff_array.flat[i] < 0: pos_neg[i] = -1

    # file_name = './result/debug/diff_array.txt'
    # diff_array_file = open(file_name, 'w')
    # for item in diff_array.flat:
    #     diff_array_file.write("%s " % item)
    # diff_array_file.close()

    # print('the time of optimize_pos_neg:', time.time() - time_begin)

    return pos_neg  # return the latest indicator of positive and negative partitions


def split_node(node,feature_mat,label_mat,wt_nDCG,norm_nDCG_array):

    instances = node.instances
    feature_spa_mat = feature_mat[:,instances] # create the sparse matrix of the current feature space
    label_spa_mat = label_mat[:,instances] # create the sparse matrix of the current label space
    norm_nDCG = norm_nDCG_array[instances]

    ndcg = -1  # define ndcg and initialize with -1
    new_ndcg = 0  # define new_ndcg and initialize with 0
    split_success = True

    (label_num_row,label_num_col) = label_spa_mat.shape

    pos_neg = numpy.random.randint(100,size=label_num_col) % 2 * 2 - 1  # # generate initial partition indicators randomly
    # pos_neg = [-1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1]  # initial partition indicators
    # pos_neg = numpy.array([i % 2 * 2 - 1 for i in range(label_num_col)])

    time_begin = time.time()

    iterator = 0
    while True:
        iterator += 1
        global global_index_tree, global_index_node
        # print('tree: ', global_index_tree, ' node: ', global_index_node, ' rank and optimize: ', iterator)

        (pos_indices_dic, neg_indices_dic, pos_desc, neg_desc) = optimize_rank(label_spa_mat, pos_neg, norm_nDCG)

        new_ndcg = numpy.dot(wt_nDCG, numpy.array(pos_desc) + numpy.array(neg_desc)) / label_num_col

        if new_ndcg - ndcg < 1e-6: break
        else: ndcg = new_ndcg

        optimize_pos_neg(label_spa_mat, pos_indices_dic, neg_indices_dic, norm_nDCG, pos_neg)

        # file_name = './result/debug/pos_neg_{}'.format(iterator)+'.txt'
        # pos_neg_file = open(file_name, 'w')
        # for item in pos_neg:
        #     pos_neg_file.write("%s\n" % item)
        # pos_neg_file.close()
    # print('the time of optimize ndcg:', time.time() - time_begin)

    # if all the instances are split to either positive partition or negative partition
    if operator.eq(pos_neg.tolist(),[1] * len(pos_neg.tolist())) | operator.eq(pos_neg.tolist(),[-1] * len(pos_neg.tolist())):
        return False, pos_neg, []

    # print('pos_neg:',pos_neg)
    # file_name = './result/debug/pos_neg.txt'
    # pos_neg_file = open(file_name, 'w')
    # for item in pos_neg:
    #     pos_neg_file.write("%s\n" % item)
    # pos_neg_file.close()


    time_begin = time.time()
    # print('LogisticRegression')
    clf_l1_LR = LogisticRegression(penalty='l1', tol=1e-4)
    clf_l1_LR.fit(feature_spa_mat.transpose(), pos_neg)
    # w_array = clf_l1_LR.coef_
    w_array = clf_l1_LR

    predict_result = clf_l1_LR.predict(feature_spa_mat.transpose())

    pos_neg = predict_result

    if operator.eq(pos_neg.tolist(),[1] * len(pos_neg.tolist())) | operator.eq(pos_neg.tolist(),[-1] * len(pos_neg.tolist())):
        return False, pos_neg, []

    # file_name = './result/debug/w.txt'
    # w_file = open(file_name, 'w')
    # for item in w_array.flat:
    #     w_file.write("%s\n" % item)
    # w_file.close()

    # print('the time of LogisticRegression:', time.time() - time_begin)
    # print(type(w_array))
    # print(w_array)
    # product = numpy.multiply(feature_spa_mat.transpose(), w_array)
    # for i in range(label_num_col):
    #     instance = feature_spa_mat.getcol(i).toarray().transpose().flatten()
    #     product = numpy.dot(instance, w_array.flatten())
    #     if numpy.dot(instance, w_array.flatten()) > 0: pos_neg[i] = 1
    #     else: pos_neg[i] = -1
    #
    # file_name = './result/debug/pos_neg_predict.txt'
    # pos_neg_file = open(file_name, 'w')
    # for item in pos_neg:
    #     pos_neg_file.write("%s\n" % item)
    # pos_neg_file.close()
    #
    # file_name = './result/debug/l_g_predict.txt'
    # l_g_pos_neg_file = open(file_name, 'w')
    # for item in predict_result:
    #     l_g_pos_neg_file.write("%s\n" % item)
    # l_g_pos_neg_file.close()


    return True, pos_neg, w_array


def cal_leaf_prob(label_mat,leaf_node):

    time_begin = time.time()
    # for i in range(1):
    #     label_spa_mat = label_mat[:, leaf_node.instances]  # create the sparse matrix of the label space of the leaf node
    #     # print('calculate the leaf node:')
    #     # print(label_spa_mat)
    #
    #     label_prob_matrix = numpy.sum(label_spa_mat, axis = 1) / len(leaf_node.instances)
    #     label_prob = numpy.array(label_prob_matrix).flatten()
    #
    # print('the time of cal_leaf_prob sum:', time.time() - time_begin)

    time_begin = time.time()
    for i in range(1):
        array = numpy.ones((len(leaf_node.instances)))
        label_spa_mat = label_mat[:, leaf_node.instances]  # create the sparse matrix of the label space of the leaf node
        label_prob = label_spa_mat.dot(array) / len(leaf_node.instances)
    # print('the time of cal_leaf_prob dot:', time.time() - time_begin)

    # print(type(label_prob))
    # print(label_prob)
    # label_indices = argsort(-label_prob).flatten().tolist()  # sort the array in descending order and get the indices
    # label_desc = sorted(label_prob.flatten(), reverse=True)  # sort the array in descending order
    # print(label_indices)
    # print(label_desc)
    # print(type(label_indices))
    # print(type(label_desc))
    # return label_indices, label_desc
    return label_prob


def train_tree(feature_mat,label_mat):
    """
    This function learns a tree classifier
    :param feature_mat: the feature space of training data
    :param label_mat:   the label space of training data
    :return nodes_list: return a node list of the learnt tree
    """
    max_leaf = 10
    (feature_num_row, feature_num_col) = feature_mat.shape
    (label_num_row, label_num_col) = label_mat.shape
    # print('feature_mat:',feature_mat)
    # print('label_mat:',  label_mat)
    original_label_mat = label_mat

    instances = list(range(feature_num_col)) # initialize the indices of root node
    nodes_list = list()  # define the node list
    nodes_list.append(Node(instances, 0, max_leaf)) # add the root node to the node list

    wt_nDCG = 1 / numpy.log2(2 + numpy.array(range(label_num_row))) # the weight of nDCG

    col_num_nonzero = numpy.array((label_mat != 0).sum(0)).ravel()
    norm_nDCG = numpy.array([1.0 / numpy.sum(wt_nDCG[:col_num_nonzero[i]]) if col_num_nonzero[i] else 0 for i in range(label_num_col)])
    # time_begin = time.time()
    # norm_nDCG = []
    # for i in range(0, label_num_col):
    #     if col_num_nonzero[i]:
    #         num = col_num_nonzero[i]
    #         norm_nDCG.append(1.0 / numpy.sum(wt_nDCG[:num]))
    #     else:
    #         norm_nDCG.append(0)
    #
    # print('the time of norm_nDCG:', time.time() - time_begin)

    # print('type of wt_nDCG:',type(wt_nDCG))
    index_node = 0
    for node in nodes_list:
        index_node += 1
        global global_index_node, global_index_tree
        global_index_node = index_node
        print('tree: ', global_index_tree, ' node: ', global_index_node)

        if index_node > 10000:
            index_node += 1
            index_node -= 1



        if node.is_leaf:  # deal with the leaf node
            node.neg_child,node.pos_child = -1,-1
            node.leaf_prob = cal_leaf_prob(label_mat, node)
            # deal with the leaf node
        else:    # split the node
            global global_num_split
            print('global_num_split: ', global_num_split)
            # label_mat = calculate_coeff_label_matrix(original_label_mat,global_num_split)

            split_success, pos_neg, w_array = split_node(node, feature_mat, label_mat, wt_nDCG, norm_nDCG) # split the node
            global_num_split+=1
            if split_success:  # split the node successfully
                # print(node.instances)
                # print(pos_neg)
                # node.w = w_array.tolist()   # assign w
                node.w = w_array
                # get the indices of positive partition
                pos_instances = [index for index in node.instances if pos_neg[node.instances.index(index)] == 1]
                # get the indices of negative partition
                neg_instances = [index for index in node.instances if pos_neg[node.instances.index(index)] == -1]
                nodes_list.append(Node(pos_instances, node.depth + 1, max_leaf)) # add the positive child node to the node list
                node.pos_child = len(nodes_list) - 1  # assign the index of positive node
                nodes_list.append(Node(neg_instances, node.depth + 1, max_leaf)) # add the negative child node to the node list
                node.neg_child = len(nodes_list) - 1  # assign the index of negative node
            else:   # the failure of splitting the node
                node.is_leaf = True
                node.neg_child, node.pos_child = -1, -1
                node.leaf_prob = cal_leaf_prob(label_mat, node)
                # deal with the leaf node

    return nodes_list


def train_trees(feature_mat, label_mat, data_set_name, param):

    file_dir = os.getcwd() + '/result/' + data_set_name + '/model/'
    if not os.path.exists(file_dir):
        os.makedirs(file_dir)

    for i in range(param):

        print('training tree ', i+1)
        global global_index_tree
        global_index_tree = i+1
        global global_num_split
        global_num_split = 0
        tree = Tree(train_tree(feature_mat, label_mat))
        tree_name = file_dir + 'tree_' + str(i + 1)
        tree.write_tree(tree_name)


def test_trees(test_mat, label_mat, data_set_name, param):
    # param = 50
    # file_dir = './result/Bibtex/model/'

    file_dir = os.getcwd() + '/result/' + data_set_name + '/model/'
    if not os.path.exists(file_dir):
        os.makedirs(file_dir)

    (test_row, test_col) = test_mat.shape
    (label_row, label_col) = label_mat.shape

    # predict_trees_matrix = csc_matrix((0,0))
    predict_trees_result = numpy.zeros((label_row, label_col))
    for i in range(param):
        tree_name = file_dir + 'tree_' + str(i+1)
        tree = Tree([]).read_tree(tree_name)
        # print(node)
        # predict_tree_matrix = csc_matrix((0,0))
        # print(type(predict_tree_matrix))
        # print(predict_tree_matrix)
        predict_tree_result = numpy.zeros((label_row, label_col))

        node = tree.nodes[0]
        instances_indices = numpy.array(range(test_col)).reshape(1, test_col)
        # print('testing...',' tree: ', i+1)
        test_node(tree, node, test_mat, instances_indices, predict_tree_result)



        # for j in range(test_col):
        #     print('testing... ','tree: ', i+1, ' instance: ', j+1)
        #
        #     instance = test_mat.getcol(j).toarray().transpose().flatten()
        #     # print(type(instance))
        #     # print(instance)
        #     node = tree.nodes[0]
        #     # print(type(node.w))
        #     # print(node.w)
        #     # print(numpy.array(node.w).flatten())
        #     # print(numpy.dot(instance, numpy.array(node.w).flatten()))
        #     # test_dot = numpy.dot(instance, numpy.array(node.w).flatten())
        #     # print(test_dot)
        #
        #     while not node.is_leaf:
        #         if numpy.dot(instance, numpy.array(node.w).flatten()) > 0: node = tree.nodes[node.pos_child]
        #         else : node = tree.nodes[node.neg_child]
        #     if not len(predict_tree_result): predict_tree_result = node.leaf_prob
        #     else: predict_tree_result = numpy.c_[predict_tree_result, node.leaf_prob]

        predict_trees_result += predict_tree_result
    predict_trees_result /= param

    # print(predict_trees_result)
    return predict_trees_result


def eval_result(predict_mat, truth_mat, data_set_name):

    # predict_mat = numpy.array([[0.6, 0.3, 0.2, 0.4], [0.3, 0.8, 0.9, 0.5], [0.2, 0.1, 0.4, 0.7], [0.1, 0.5, 0.3, 0.2], [0.9, 0.6, 0.1, 0.9]])
    # truth_mat = numpy.array([[1, 0, 1, 0], [0, 1, 1, 1], [0, 0, 1, 0], [1, 1, 0, 0], [0, 1, 0, 0]])

    (predict_row, predict_col) = predict_mat.shape

    indices_array = numpy.argsort(-predict_mat, axis=0)

    mean_precision_list = []
    for k in range(1,6,2):  # k = 1, 3, 5
        k_indices_aray = indices_array[0:k, :]

        mat_indices_list = []
        precision_list = []
        for i in range(predict_col):
            mat_index = [(col, i) for col in k_indices_aray[:, i]]
            mat_indices_list += mat_index
            # precision_list
        mean_precision = numpy.sum(truth_mat[i] for i in mat_indices_list) / k / predict_col


        # for i in range(predict_col):
        #     precision_instance = numpy.sum(truth_mat[k_indices_aray[:, i], i]) / k
        #     precision_list.append(precision_instance)
        # mean_precision = numpy.sum(precision_list) / predict_col
        mean_precision_list.append((k,mean_precision))

    for element in mean_precision_list:  # k = 1, 3, 5
        print('P',element[0],': ',element[1])

    eval_result_file = open('./result/{}/eval_result.txt'.format(data_set_name),'w')
    for element in mean_precision_list:  # k = 1, 3, 5
        eval_result_file.write('P{} : {}\n'.format(element[0], element[1]))
    eval_result_file.close()


def test_node(tree, node, instances_mat, instances_indices, predict_tree_result):
    (num_row, num_col) = instances_mat.shape
    if node.is_leaf :
        for i in instances_indices.flatten():
            predict_tree_result[:, i] = node.leaf_prob
    else :
        # dot_result = instances_mat.transpose().dot(numpy.array(node.w).reshape(num_row, 1))
        # pos_neg = numpy.array([i % 2 * 2 - 1 for i in range(num_col)])
        # clf_l1_LR = LogisticRegression(penalty='l1', tol=1e-4)
        # clf_l1_LR.fit(instances_mat.transpose(), pos_neg)
        # dot_result = clf_l1_LR.predict(instances_mat.transpose()).reshape(num_col, 1)

        # print(type(node.w))
        dot_result = node.w.predict(instances_mat.transpose()).reshape(num_col, 1)

        pos_indices = [index for index in range(num_col) if dot_result[index] > 0]
        neg_indices = [index for index in range(num_col) if dot_result[index] <= 0]

        if pos_indices:
            pos_instances_mat = instances_mat[:, pos_indices]
            pos_instances_indices = instances_indices[:, pos_indices]
            pos_child_node = tree.nodes[node.pos_child]
            test_node(tree, pos_child_node, pos_instances_mat, pos_instances_indices, predict_tree_result)
        if neg_indices:
            neg_instances_mat = instances_mat[:, neg_indices]
            neg_instances_indices = instances_indices[:, neg_indices]
            neg_child_node = tree.nodes[node.neg_child]
            test_node(tree, neg_child_node, neg_instances_mat, neg_instances_indices, predict_tree_result)

        # pos_instances_mat = instances_mat[:, pos_indices]
        # neg_instances_mat = instances_mat[:, neg_indices]
        # pos_instances_indices = instances_indices[:, pos_indices]
        # neg_instances_indices = instances_indices[:, neg_indices]
        # pos_child_node = tree.nodes[node.pos_child]
        # neg_child_node = tree.nodes[node.neg_child]
        #
        # test_node(tree, pos_child_node, pos_instances_mat, pos_instances_indices, predict_tree_result)
        # test_node(tree, neg_child_node, neg_instances_mat, neg_instances_indices, predict_tree_result)


def multiply_inv_pro(label_mat,file_name):

    (label_row, label_col) = label_mat.shape
    temp_mat = label_mat.copy()

    file = open(file_name, 'r')
    lines = file.readlines()
    value_list = []
    for line in lines:
        value = float(line.strip())
        value_list.append(value)

    inv_pro_matrix = numpy.array(value_list*label_col).reshape(label_col,label_row).transpose()

    print(inv_pro_matrix)

    label_pro_mat = temp_mat.multiply(inv_pro_matrix)

    return label_pro_mat.tocsc()

    # print(label_mat)


def calculate_inv_pro(label_mat,file_name):
    param_a = 0.55
    param_b = 1.5

    param_power = 750
    global global_array

    (label_row, label_col) = label_mat.shape
    # param_c = (numpy.log(label_col)-1)*numpy.power(param_b+1,param_a)
    #
    # label_number = label_mat.sum(axis = 1)
    #
    # label_inv_pro = numpy.array([(1 + param_c * numpy.power((number + param_b),-0.55)) for number in label_number])
    #
    # label_inv_temp = label_inv_pro.flatten()
    num_label_array = numpy.array(label_mat.sum(axis = 1)).flatten()

    most_frequent_num = numpy.amax(num_label_array)

    label_inv_temp  = numpy.array([most_frequent_num/i if i else 1 for i in num_label_array])
    index_array = argsort(label_inv_temp)
    index = index_array[0]
    label_inv_temp[index] = most_frequent_num

    # label_inv_temp  = numpy.array([numpy.sqrt(most_frequent_num/i)+3 if i else 0 for i in num_label_array])
    #label_inv_temp  = numpy.array([numpy.log(most_frequent_num/i)+1 if i else 0 for i in num_label_array])
    # label_inv_temp  = numpy.array([numpy.log2(most_frequent_num/i) if i else 0 for i in num_label_array])
    # label_inv_temp  = numpy.array([numpy.power(most_frequent_num/i,0) if i else 1 for i in num_label_array])

    global_array = numpy.array([numpy.power(1.0/entry,1.0/param_power) for entry in label_inv_temp])

    inv_pro_file = open(file_name, 'w')
    for element in label_inv_temp:
        inv_pro_file.write('{}\n'.format(element))

    inv_pro_file.close()


def calculate_coeff_label_matrix(label_mat,split_num):
    # param_a = 0.55
    # param_b = 1.5
    # param_power = 800
    global global_array

    (label_row, label_col) = label_mat.shape

    # param_c = (numpy.log(label_col)-1)*numpy.power(param_b+1,param_a)
    #
    # label_number = label_mat.sum(axis = 1)
    #
    # label_inv_pro = numpy.array([(1 + param_c * numpy.power((number + param_b),-0.55)) for number in label_number])
    #
    # label_inv_temp = label_inv_pro.flatten()
    num_label_array = numpy.array(label_mat.sum(axis = 1)).flatten()

    most_frequent_num = numpy.amax(num_label_array)

    # label_inv_temp  = numpy.array([most_frequent_num/i if i else 0 for i in num_label_array])
    # label_inv_temp  = numpy.array([numpy.sqrt(most_frequent_num/i)+3 if i else 0 for i in num_label_array])
    #label_inv_temp  = numpy.array([numpy.log(most_frequent_num/i)+1 if i else 0 for i in num_label_array])
    # label_inv_temp  = numpy.array([numpy.log2(most_frequent_num/i) if i else 0 for i in num_label_array])
    # label_inv_temp  = numpy.array([numpy.power(most_frequent_num/(1*i),1.0/param_power) if i else 0 for i in num_label_array])

    label_coeffi_array = numpy.power(global_array,split_num)

    # new_label_mat = numpy.dot(numpy.diag(label_coeffi_array),label_mat.toarray())
    new_label_mat = csc_matrix(numpy.diag(label_coeffi_array)).dot(label_mat)

    return  new_label_mat

    # inv_pro_file = open(file_name, 'w')
    # for element in label_inv_temp:
    #     inv_pro_file.write('{}\n'.format(element))
    #
    # inv_pro_file.close()


def train_trees2(feature_mat, label_mat, old_label_mat,data_set_name, param):

    file_dir = os.getcwd() + '/result/' + data_set_name + '/model/'
    if not os.path.exists(file_dir):
        os.makedirs(file_dir)

    for i in range(param):

        print('training tree ', i+1)
        global global_index_tree
        global_index_tree = i+1
        global global_num_split
        global_num_split = 0
        tree = Tree(train_tree2(feature_mat, label_mat,old_label_mat))
        tree_name = file_dir + 'tree_' + str(i + 1)
        tree.write_tree(tree_name)


def train_tree2(feature_mat,label_mat,old_label_mat):
    """
    This function learns a tree classifier
    :param feature_mat: the feature space of training data
    :param label_mat:   the label space of training data
    :return nodes_list: return a node list of the learnt tree
    """
    max_leaf = 10
    (feature_num_row, feature_num_col) = feature_mat.shape
    (label_num_row, label_num_col) = label_mat.shape
    # print('feature_mat:',feature_mat)
    # print('label_mat:',  label_mat)
    original_label_mat = label_mat

    instances = list(range(feature_num_col)) # initialize the indices of root node
    nodes_list = list()  # define the node list
    nodes_list.append(Node(instances, 0, max_leaf)) # add the root node to the node list

    wt_nDCG = 1 / numpy.log2(2 + numpy.array(range(label_num_row))) # the weight of nDCG

    col_num_nonzero = numpy.array((label_mat != 0).sum(0)).ravel()
    norm_nDCG = numpy.array([1.0 / numpy.sum(wt_nDCG[:col_num_nonzero[i]]) if col_num_nonzero[i] else 0 for i in range(label_num_col)])
    # time_begin = time.time()
    # norm_nDCG = []
    # for i in range(0, label_num_col):
    #     if col_num_nonzero[i]:
    #         num = col_num_nonzero[i]
    #         norm_nDCG.append(1.0 / numpy.sum(wt_nDCG[:num]))
    #     else:
    #         norm_nDCG.append(0)
    #
    # print('the time of norm_nDCG:', time.time() - time_begin)

    # print('type of wt_nDCG:',type(wt_nDCG))
    index_node = 0
    for node in nodes_list:
        index_node += 1
        global global_index_node, global_index_tree
        global_index_node = index_node
        print('tree: ', global_index_tree, ' node: ', global_index_node)

        if index_node > 10000:
            index_node += 1
            index_node -= 1



        if node.is_leaf:  # deal with the leaf node
            node.neg_child,node.pos_child = -1,-1
            node.leaf_prob = cal_leaf_prob(old_label_mat, node)
            # deal with the leaf node
        else:    # split the node
            global global_num_split
            print('global_num_split: ', global_num_split)
            # label_mat = calculate_coeff_label_matrix(original_label_mat,global_num_split)

            split_success, pos_neg, w_array = split_node(node, feature_mat, label_mat, wt_nDCG, norm_nDCG) # split the node
            global_num_split+=1
            if split_success:  # split the node successfully
                # print(node.instances)
                # print(pos_neg)
                # node.w = w_array.tolist()   # assign w
                node.w = w_array
                # get the indices of positive partition
                pos_instances = [index for index in node.instances if pos_neg[node.instances.index(index)] == 1]
                # get the indices of negative partition
                neg_instances = [index for index in node.instances if pos_neg[node.instances.index(index)] == -1]
                nodes_list.append(Node(pos_instances, node.depth + 1, max_leaf)) # add the positive child node to the node list
                node.pos_child = len(nodes_list) - 1  # assign the index of positive node
                nodes_list.append(Node(neg_instances, node.depth + 1, max_leaf)) # add the negative child node to the node list
                node.neg_child = len(nodes_list) - 1  # assign the index of negative node
            else:   # the failure of splitting the node
                node.is_leaf = True
                node.neg_child, node.pos_child = -1, -1
                node.leaf_prob = cal_leaf_prob(old_label_mat, node)
                # deal with the leaf node

    return nodes_list


def eval_infrequent_result_2(train_label_matrix, predict_mat, truth_mat, data_set_name, a):
    # predict_mat = numpy.array([[0.6, 0.3, 0.2, 0.4], [0.3, 0.8, 0.9, 0.5], [0.2, 0.1, 0.4, 0.7], [0.1, 0.5, 0.3, 0.2], [0.9, 0.6, 0.1, 0.9]])
    # truth_mat = numpy.array([[1, 0, 1, 0], [0, 1, 1, 1], [0, 0, 1, 0], [1, 1, 0, 0], [0, 1, 0, 0]])
    # train_label_matrix = numpy.array([[1, 0, 1, 0], [0, 1, 1, 1], [0, 0, 1, 0], [1, 1, 0, 0], [0, 1, 0, 0]])
    #
    # a = 1

    (predict_row, predict_col) = predict_mat.shape

    indices_array = numpy.argsort(-predict_mat, axis=0)
    # print(indices_array)

    # calculate the indices of infrequent label to be evaluated
    (train_row, train_col) = train_label_matrix.shape
    frequency_array = numpy.array(train_label_matrix.sum(axis=1)).flatten()
    index_array = argsort(frequency_array)
    num_infrequent_label = int(train_row * a)
    index_infrequent_label = index_array[0:num_infrequent_label]

    # truth_mat
    infrequent_array = numpy.zeros(predict_row)
    infrequent_array[index_infrequent_label] = 1
    diag_matrix = csc_matrix((predict_row, predict_row))
    diag_matrix.setdiag(infrequent_array)
    # print(diag_matrix.toarray())
    infrequent_matrix = diag_matrix.dot(truth_mat)
    infrequent_true_array = numpy.array(infrequent_matrix.sum(axis=0)).flatten()

    k_list = [1,3,5]

    mean_precision_list = []

    for k in k_list:
        k_indices_array = indices_array[0:k, :]
        row_indices_list = k_indices_array.transpose().flatten().tolist()
        temp_array = numpy.array(list(range(predict_col))*k).reshape(k,predict_col)
        col_indices_list = temp_array.transpose().flatten().tolist()

        predict_value_array = truth_mat[row_indices_list, col_indices_list]
        # temp_test =
        infrequent_indices_array = numpy.array([index in index_infrequent_label for index in row_indices_list])
        # temp_value_array = numpy.multiply(predict_value_array,infrequent_indices_array)

        temp_true_array = infrequent_true_array.copy()
        total_infrequent_array = numpy.array([k if temp_true_array[i] >= k else temp_true_array[i] for i in range(predict_col)])
        num_total_infrequent = numpy.sum(total_infrequent_array)
        mean_precision = numpy.sum(numpy.multiply(predict_value_array,infrequent_indices_array)) / num_total_infrequent



        mean_precision_list.append((k, mean_precision))

    for element in mean_precision_list:  # k = 1, 3, 5
        print('P', element[0], ': ', element[1])

    eval_result_file = open('./result/{}/eval_result_infrequent.txt'.format(data_set_name), 'w')
    for element in mean_precision_list:  # k = 1, 3, 5
        eval_result_file.write('P{} : {}\n'.format(element[0], element[1]))
    eval_result_file.close()


def statistic_leaf_node2(data_set_name, param, train_label_matrix, truth_label_matrix):

    file_dir = os.getcwd() + '/result/' + data_set_name + '/model/'
    if not os.path.exists(file_dir):
        os.makedirs(file_dir)

    (row, col) = truth_label_matrix.shape

    train_frequency_array = numpy.array(train_label_matrix.sum(axis=1)).flatten()
    index_train = argsort(-train_frequency_array)

    truth_frequency_array = numpy.array(truth_label_matrix.sum(axis=1)).flatten()
    index_truth = argsort(-truth_frequency_array)

    num_label_p1_list = list([0.0]) * row
    num_instance_p1_list = list([0.0]) * row
    num_ave_instance_p1_list = list([0.0]) * row

    num_instance_label_p1_list = list([0.0]) * row


    for i in range(param):
        tree_name = file_dir + 'tree_' + str(i + 1)
        tree = Tree([]).read_tree(tree_name)

        for node in tree.nodes:
            if node.is_leaf:
                index_array = argsort(-node.leaf_prob)
                temp_prob = numpy.array([1.0 if node.leaf_prob[i] == 1.0 else 0 for i in range(len(node.leaf_prob))])
                # temp_prob =
                index_nonzeros = numpy.nonzero(temp_prob)[0]
                index_p1_set = set(index_nonzeros)
                index_p1 = index_array[0]
                index_p1_set = set([])
                index_p1_set.add(index_p1)
                for index_p1 in index_p1_set:
                    if index_p1 == 131 and i == 1:
                        # print('node: ',index_p1)
                        print(len(node.instances))
                        # for instance in node.instances:

                        # print(numpy.nonzero(train_label_matrix[:,instance])[0])


                    num_label_p1_list[index_p1] += 1.0
                    num_instance_p1_list[index_p1] += len(node.instances)
                    for instance in node.instances:
                        if train_label_matrix[index_p1,instance]:
                            num_instance_label_p1_list[index_p1] += 1.0
        test = 0
        test +=1

    num_label_p1_array = numpy.array(num_label_p1_list) / param
    num_instance_p1_array = numpy.array(num_instance_p1_list) / param
    num_instance_label_p1_array = numpy.array(num_instance_label_p1_list) / param

    num_label_p1_list = num_label_p1_array.tolist()
    num_ave_instance_p1_list = [num_instance_p1_array[i]/num_label_p1_array[i] if num_label_p1_array[i] else 0 for i in range(row)]
    precision_label_p1_list = [num_instance_label_p1_array[i]/num_instance_p1_array[i] if num_instance_p1_array[i] else 0 for i in range(row)]

    num_ave_instance = numpy.sum(num_instance_p1_array)/numpy.sum(num_label_p1_array)

    file_dir = os.getcwd() + '/result/' + data_set_name + '/model/'
    file_name = file_dir + 'statistic_leaf3.txt'
    file = open(file_name, 'w')

    for j in index_truth:
        # print(train_frequency_array[j],' ',truth_frequency_array[j],' ',num_label_p1_list[j])
        temp1 = int(train_frequency_array[j])
        temp2 = int(truth_frequency_array[j])
        temp3 = int(num_label_p1_list[j])
        temp4 = num_instance_p1_array[j]
        temp5 = num_ave_instance_p1_list[j]
        temp6 = precision_label_p1_list[j]
        file.write('{0:<8}{1:<8}{2:<8}{3:<8}{4:<8.2f}{5:<8.2f}{6:<8.2f}\n'.format(temp1, temp2, temp3, temp4, temp5, temp6, num_ave_instance))
    file.close()
    return num_label_p1_list


def eval_result_label(predict_mat, truth_mat, data_set_name):

    # predict_mat = numpy.array([[0.6, 0.3, 0.2, 0.4], [0.3, 0.8, 0.9, 0.5], [0.2, 0.1, 0.4, 0.7], [0.1, 0.5, 0.3, 0.2], [0.9, 0.6, 0.1, 0.9]])
    # truth_mat = numpy.array([[1, 0, 1, 0], [0, 1, 1, 1], [0, 0, 1, 0], [1, 1, 0, 0], [0, 1, 0, 0]])

    (predict_row, predict_col) = predict_mat.shape

    truth_frequency_array = numpy.array(truth_mat.sum(axis=1)).flatten()
    index_true = argsort(-truth_frequency_array)


    indices_array = numpy.argsort(-predict_mat, axis=0)

    num_predict_label = list([0])*predict_row
    num_true_label = list([0])*predict_row

    for k in range(1,2,1):  # k = 1, 3, 5
        k_indices_aray = indices_array[0:k, :]
        row_index = k_indices_aray.flatten()
        for i in row_index:
            num_predict_label[i] += 1

        col_index = numpy.array(range(predict_col))
        true_label_array = truth_mat.toarray()[row_index,col_index].flatten()
        non_zero_index = numpy.nonzero(true_label_array)[0]

        for j in non_zero_index:
            num_true_label[row_index[j]] += 1

        precision = non_zero_index.size * 1.0 / col_index.size
        # print('precision: ',precision)

    file_dir = os.getcwd() + '/result/' + data_set_name + '/model/'
    file_name = file_dir + 'precision_recall.txt'
    file = open(file_name, 'w')

    for i in index_true:
        if num_predict_label[i] and truth_frequency_array[i]:
            num_frequency = int(truth_frequency_array[i])
            num_true = num_true_label[i]
            num_predict = num_predict_label[i]
            label_precision = num_true_label[i] * 1.0 / num_predict_label[i]
            label_recall = num_true_label[i] * 1.0 / truth_frequency_array[i]
            # print(i, ' ',truth_frequency_array[i],' ',num_predict_label[i],' ',num_true_label[i],' ',label_precision,' ',label_recall)
            file.write('{0:<8}{1:<8}{2:<8}{3:<8}{4:<8.2f}{5:<8.2f}\n'.format(i,num_frequency,num_predict,num_true,label_precision,label_recall))
        else:
            # print(i, ' ',truth_frequency_array[i],' ',num_predict_label[i], ' ', num_true_label[i])
            num_frequency = int(truth_frequency_array[i])
            num_true = num_true_label[i]
            num_predict = num_predict_label[i]
            label_precision = 0
            label_recall =0
            file.write('{0:<8}{1:<8}{2:<8}{3:<8}{4:<8}{5:<8}\n'.format(i, num_frequency, num_predict, num_true,label_precision,label_recall))
    file.close()


def train_trees3(feature_mat, label_mat, test_feature_mat, test_label_mat, data_set_name, param):

    file_dir = os.getcwd() + '/result/' + data_set_name + '/model/'
    if not os.path.exists(file_dir):
        os.makedirs(file_dir)

    row, col = label_mat.shape
    new_train_label_mat = label_mat

    label_weights = numpy.ones(row)
    file_name = file_dir + 'weight_' + str(0)
    file = open(file_name, 'w')
    for element in label_weights:
        file.write('{}\n'.format(element))
    file.close()

    for i in range(param):

        print('training tree ', i+1)
        global global_index_tree
        global_index_tree = i+1
        global global_num_split
        global_num_split = 0
        # tree = Tree(train_tree(feature_mat, label_mat))
        tree = Tree(train_tree(feature_mat, new_train_label_mat))
        tree_name = file_dir + 'tree_' + str(i + 1)
        tree.write_tree(tree_name)

        test_result = test_trees3(test_feature_mat, test_label_mat, data_set_name, i+1)
        # label_weights = calculate_label_weight(test_label_mat,test_result)
        label_weights = calculate_label_weight2(test_label_mat, test_result)
        file_name = file_dir + 'weight_'+ str(i)
        # weight_file = open(weight_name, 'r')
        file = open(file_name, 'r')
        lines = file.readlines()
        value_list = []
        for line in lines:
            value = float(line.strip())
            value_list.append(value)
        pre_label_weights = numpy.array(value_list)

        label_weights = numpy.multiply(label_weights,pre_label_weights)

        file_name = file_dir + 'weight_' + str(i+1)
        file = open(file_name, 'w')
        for element in label_weights:
            file.write('{}\n'.format(element))

        file.close()


        temp_matrix = csc_matrix((row,row))
        temp_matrix.setdiag(label_weights)

        new_train_label_mat = temp_matrix.dot(label_mat)





def test_trees3(test_mat, label_mat, data_set_name, param):
    # param = 50
    # file_dir = './result/Bibtex/model/'

    file_dir = os.getcwd() + '/result/' + data_set_name + '/model/'
    if not os.path.exists(file_dir):
        os.makedirs(file_dir)

    (test_row, test_col) = test_mat.shape
    (label_row, label_col) = label_mat.shape

    # predict_trees_matrix = csc_matrix((0,0))
    predict_trees_result = numpy.zeros((label_row, label_col))
    for i in range(param):
    # for i in range(1):
        tree_name = file_dir + 'tree_' + str(i+1)
        # tree_name = file_dir + 'tree_' + str(param + 1)
        tree = Tree([]).read_tree(tree_name)
        # print(node)
        # predict_tree_matrix = csc_matrix((0,0))
        # print(type(predict_tree_matrix))
        # print(predict_tree_matrix)
        predict_tree_result = numpy.zeros((label_row, label_col))

        node = tree.nodes[0]
        instances_indices = numpy.array(range(test_col)).reshape(1, test_col)
        # print('testing...',' tree: ', i+1)
        test_node(tree, node, test_mat, instances_indices, predict_tree_result)



        # for j in range(test_col):
        #     print('testing... ','tree: ', i+1, ' instance: ', j+1)
        #
        #     instance = test_mat.getcol(j).toarray().transpose().flatten()
        #     # print(type(instance))
        #     # print(instance)
        #     node = tree.nodes[0]
        #     # print(type(node.w))
        #     # print(node.w)
        #     # print(numpy.array(node.w).flatten())
        #     # print(numpy.dot(instance, numpy.array(node.w).flatten()))
        #     # test_dot = numpy.dot(instance, numpy.array(node.w).flatten())
        #     # print(test_dot)
        #
        #     while not node.is_leaf:
        #         if numpy.dot(instance, numpy.array(node.w).flatten()) > 0: node = tree.nodes[node.pos_child]
        #         else : node = tree.nodes[node.neg_child]
        #     if not len(predict_tree_result): predict_tree_result = node.leaf_prob
        #     else: predict_tree_result = numpy.c_[predict_tree_result, node.leaf_prob]

        predict_trees_result += predict_tree_result
    # predict_trees_result /= param
    predict_trees_result /= 1

    # print(predict_trees_result)
    return predict_trees_result


def calculate_ndcg0(label_index_array,label_array):

    (row, col) = label_array.shape

    index_row_array = numpy.repeat(label_index_array.reshape(1,row),col,axis=0).flatten()
    index_col_array = numpy.repeat(numpy.array(range(col)),row)
    # index_col_array = numpy.array(list(range(col))*row).reshape(row,col).transpose().flatten()
    temp_label_array = label_array[index_row_array,index_col_array].reshape(col,row).transpose()
    # print(temp_label_array)

    dcg_array = numpy.array([1/log2(2+i) for i in range(row)])

    label_frequency_array = numpy.sum(label_array,axis=0).flatten().astype(int)
    norm_coefficient_array = numpy.array([1.0/numpy.sum(dcg_array[range(i)]) if i else 1 for i in label_frequency_array])
    # print(dcg_array)
    # print(label_frequency_array)
    # print(ndcg_coefficient_array)
    ndcg_array = numpy.multiply(numpy.dot(dcg_array,temp_label_array),norm_coefficient_array)

    return ndcg_array


def calculate_ndcg(true_array,test_array):

    # test_array = numpy.array([1.46,2.69,2.08,0.61,0])
    # true_array = numpy.array([1.0, 0, 0, 1, 0])

    col = true_array.size
    index_array = argsort(-test_array)


    num_nonzeros = numpy.count_nonzero(true_array)

    temp_array = true_array[index_array]

    dcg_array = numpy.array([1 / log2(2 + i) for i in range(col)])
    norm_coefficient = numpy.sum(dcg_array)
    # norm_coefficient = numpy.sum(dcg_array[:num_nonzeros])
    # ndcg = numpy.dot(dcg_array[:num_nonzeros],temp_array[:num_nonzeros])
    ndcg = numpy.dot(dcg_array, temp_array)
    if norm_coefficient:
        ndcg /= norm_coefficient
    else:
        ndcg = 0

    return ndcg


def calculate_label_weight(test_label_mat,test_result):

    row, col = test_label_mat.shape

    weight_array = numpy.ones(row)
    for i in range(row):
        test_label_array = test_label_mat[i,:].toarray().flatten()
        test_result_array = test_result[i,:].flatten()
        ndcg = calculate_ndcg(test_label_array,test_result_array)
        if ndcg:
            weight_array[i] = 1.0 / ndcg
            # weight_array[i] = ndcg
        else:
            weight_array[i] = 1.0
            # weight_array[i] = 0.0001

    return  weight_array


def calculate_label_weight2(test_label_mat,test_result):

    row, col = test_label_mat.shape

    weight_array = numpy.ones(row)
    for i in range(row):
        test_label_array = test_label_mat[i,:].toarray().flatten()
        test_result_array = test_result[i,:].flatten()
        ndcg = calculate_ndcg(test_label_array,test_result_array)
        # if ndcg:
        if ndcg > 0.01:
            weight_array[i] = 1.0 / sqrt(ndcg)
            # weight_array[i] = ndcg
        else:
            weight_array[i] = 10
            # weight_array[i] = 1.0
            # weight_array[i] = 0.0001
    norm_coef = numpy.sum(weight_array)
    weight_array = weight_array / norm_coef

    return  weight_array