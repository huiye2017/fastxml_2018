from fastXML import *
from dataStructure import *
import time
import sys
# file = open('./Bibtex/Bibtex_data.txt', 'r')
# line = file.readline()
# print(line)

# train_feature = SparseMatrix('train_feature.txt')  # read the feature data
# train_label = SparseMatrix('train_label.txt')      # read the label data
# test_feature = SparseMatrix('test_feature.txt')
# test_label = SparseMatrix('test_feature.txt')

# train_feature = SparseMatrix('./dataset/EUR-Lex/trn_X_Xf.txt')  # read the feature data
# train_label = SparseMatrix('./dataset/EUR-Lex/trn_X_Y.txt')      # read the label data
# test_feature = SparseMatrix('./dataset/EUR-Lex/tst_X_Xf.txt')
# truth_label = SparseMatrix('./dataset/EUR-Lex/tst_X_Y.txt')
# string1 = sys.argv[0]
# string2 = sys.argv[1]
# num = sys.argv[2]
# train_feature_matrix = SparseMatrix('./dataset/Bibtex/Bibtex_train_feature.txt').matrix  # read the feature data
# train_label_matrix = SparseMatrix('./dataset/Bibtex/Bibtex_train_label.txt').matrix      # read the label data
# string1 = sys.argv[0]
# string2 = sys.argv[1]
data_list = ['Bibtex','Delicious','Mediamill']
data_list = ['Bibtex']
param = 50  # number of trees
a = 0.2

for data_set_name in data_list:
    # train_feature_matrix = SparseMatrix('./dataset/'+ data_set_name + '/' + data_set_name + '_train_feature.txt').matrix  # read the feature data
    # train_label_matrix = SparseMatrix('./dataset/'+ data_set_name + '/' + data_set_name + '_train_label.txt').matrix      # read the label data
    train_feature_matrix = SparseMatrix('/home/huy217/fastXML/dataset/'+ data_set_name + '/' + data_set_name + '_train_feature.txt').matrix  # read the feature data
    train_label_matrix = SparseMatrix('/home/huy217/fastXML/dataset/'+ data_set_name + '/' + data_set_name + '_train_label.txt').matrix      # read the label data
    test_feature_matrix = SparseMatrix('/home/huy217/fastXML/dataset/'+ data_set_name + '/' + data_set_name + '_test_feature.txt').matrix
    truth_label_matrix = SparseMatrix('/home/huy217/fastXML/dataset/'+ data_set_name + '/' + data_set_name + '_test_label.txt').matrix

    # calculate_inv_pro(train_label_matrix,'./dataset/' + data_set_name + '/inv_prop.txt')

    # new_train_label_matrix = multiply_inv_pro(train_label_matrix, './dataset/' + data_set_name + '/inv_prop.txt')

    # print(train_label_matrix)

    # test_feature_matrix = SparseMatrix('./dataset/'+ data_set_name + '/' + data_set_name + '_test_feature.txt').matrix
    # truth_label_matrix = SparseMatrix('./dataset/'+ data_set_name + '/' + data_set_name + '_test_label.txt').matrix
# time_begin = time.time()
# for i in range(1):
#     train_feature_matrix = SparseMatrix('./dataset/Bibtex/Bibtex_train_feature.txt').matrix  # read the feature data
# print('the cost of time:',time.time()-time_begin)

# data_set_name = 'Bibtex'
# data_set_name = 'EUR-Lex'

# print(feature_data.matrix)
#
# instances = [0,1,2,3,4,5,6,7,8,9,10,11]
# # instances = [0,10]
#
# # wt_nDCG = []  # the weight of nDCG
# (label_num_row,label_num_col) = label_data.matrix.shape
# wt_nDCG = 1 / numpy.log2(2 + numpy.array(range(label_num_row)))
#
# fastXML.split_node(instances,feature_data.matrix,label_data.matrix,wt_nDCG)

# fastXML.train_tree(feature_data.matrix, label_data.matrix)

    time_begin = time.time()
    train_trees3(train_feature_matrix, train_label_matrix, test_feature_matrix , truth_label_matrix, data_set_name, param)
    print('the time of training:', time.time() - time_begin)
#
    time_begin = time.time()
    test_result = test_trees(test_feature_matrix, truth_label_matrix, data_set_name, param)
    print('the time of test:', time.time() - time_begin)
# #
    time_begin = time.time()
    eval_result(test_result, truth_label_matrix, data_set_name)
    print('the time of evaluation:', time.time() - time_begin)

    time_begin = time.time()
    # eval_result(test_result, truth_label_matrix, data_set_name)
    eval_infrequent_result_2(train_label_matrix, test_result, truth_label_matrix, data_set_name, a)
    # eval_missing_result(train_label_matrix, test_result, truth_label_matrix, data_set_name, complete_label, missing_label)
    print('the time of evaluation:', time.time() - time_begin)

    statistic_leaf_node2(data_set_name, param, train_label_matrix, truth_label_matrix)

    time_begin = time.time()
    eval_result_label(test_result, truth_label_matrix, data_set_name)
    print('the time of evaluation:', time.time() - time_begin)