# import numpy as np
# import sys
# import time
# from scipy.sparse import csr_matrix
# from scipy.sparse import csc_matrix
# from math import *

# print('This is the name of the script: ', sys.argv[0])
# print('Number of arguments: ', len(sys.argv))
# print('The arguments are: ' , str(sys.argv))

# print('command line')
# matrix = np.array([[1,2,3],[1,1,1]]).reshape(3,2)
# w = np.array([[1,1,1]])
# # pos_neg = np.dot(instance, numpy.array(node.w).flatten())
# # pos_neg = np.multiply(matrix, w)
# pos_neg = np.dot(w, matrix)
# print(matrix)
# print(w)
# print(pos_neg)
# num_row = 1000000
# time_begin = time.time()
# wt_nDCG = 1 / np.log2(2 + np.array(range(num_row)))
# print('the time of numpy:',time.time()-time_begin)
#
# time_begin = time.time()
# wt_nDCG = [0] * num_row
# for i in range(num_row):
#     wt_nDCG[i] = (1 / log2(2 + i))
# print('the time of for:',time.time()-time_begin)
#
# time_begin = time.time()
# wt_nDCG = list()
# for i in range(num_row):
#     wt_nDCG.append(1 / log2(2 + i))
# print('the time of append:',time.time()-time_begin)
#
# num_row = 5
# pos_indices = [2,1,5,3,4]
# pos_indices_dic = {i:pos_indices[i] for i in range(num_row)}
#
# print(pos_indices_dic)
# for entry in pos_indices_dic:
#     print(entry)
#
# matrix = np.array([[1,2,3],[1,1,1]])
#
#
# array = np.array([[1,2,3]])
# flat = array.flatten()
# print(array)
# print(flat)

# array = np.array([[1,2,3]])
# row = np.array([0, 2, 2, 0, 1, 2])
# col = np.array([0, 0, 1, 2, 2, 2])
# data = np.array([1, 2, 3, 4, 5, 6])
# matrix = csr_matrix((data, (row, col)), shape=(3, 3))
#
# col = matrix.shape[0]
#
# product = np.dot(array, matrix.toarray())
# print(product)
#
# product = matrix.multiply(array)
# print(product)

# array = np.array([[1,2,3,4],[1,1,1,1],[1,1,1,1]])
# print(array)
# list = [0,1]
# list1 = np.array([[0,0,0],[0,0,0]]).reshape(2,3)
# print(array[:,list])
# array[:,list] = list1
# print(array)

# label_row, label_col = 3, 10
# list = [0,1]
# list1 = np.array([[1,1,1]])
# list2 = list1 * 2
# predict_tree_result = np.zeros((label_row, label_col))
# print(predict_tree_result[:,list])
# # predict_tree_result[:,list] = list1*2
# # print(predict_tree_result)
# tuple = (1,1)
# tuple_list = [(0,0),(1,1)]
# print(predict_tree_result[tuple_list])
# i = 1
# print('{}'.format(i))
# array = np.array([[1,1,1]])
# list1 = np.array([[1,1,1]]).tolist()
# print(list1)
#
# for entry in array.flat:
#     print(entry)

# import numpy
# from numpy import *
# from sklearn.datasets import load_iris  # import datasets
#
# # load the dataset: iris
# iris = load_iris()
# samples = iris.data
# # print samples
# target = iris.target
#
# # import the LogisticRegression
# from sklearn.linear_model import LogisticRegression
#
# classifier = LogisticRegression()  # 使用类，参数全是默认的
# classifier.fit(samples, target)  # 训练数据来学习，不需要返回值
#
# x = classifier.predict(numpy.array([5, 3, 5, 2.5]).reshape(1,4))  # 测试数据，分类返回标记
#
# print(x)

# list1 = []
# if list1 == []:
#     print('list is empty')

# import numpy as np
# from scipy.sparse import csc_matrix

# param_power = 4
#
# matrix1 = np.array([[1,2,3],[1,1,1],[0,0,0]])
# print(matrix1)
#
# label_mat = csc_matrix(matrix1)
# print(label_mat)
#
# num_label_array = np.array(label_mat.sum(axis=1)).flatten()
#
# most_frequent_num = np.amax(num_label_array)
#
# print(most_frequent_num)
#
# label_inv_temp = np.array([np.power(most_frequent_num / i, 1.0 / param_power) if i else 0 for i in num_label_array])
# print(label_inv_temp)
#
# label_coeffi_array = np.power(label_inv_temp,1)
# print(label_coeffi_array)
#
# new_label_mat = csc_matrix(np.diag(label_coeffi_array)).dot(label_mat)
# # new_label_mat = np.dot(np.diag(label_coeffi_array),label_mat.toarray())
# print(new_label_mat.toarray())

#
# num_label_matrix = csc_temp.sum(axis = 1)
# print(num_label_matrix)
#
# num_label_array = np.array(num_label_matrix).flatten()
# print(num_label_array)
#
# most_frequent_num = np.amax(num_label_array)
#
# print(most_frequent_num)
#
# label_inv_pro  = np.array([most_frequent_num/i if i else 0 for i in num_label_array])
#
# print(label_inv_pro)
#
# print(2.0/3)

# matrix2 = np.array([2,2,2])
# element_wise = np.multiply(matrix1, matrix2)
# matrix1.multiply(matrix2)
# print(matrix1)
# print(element_wise)
#
# matrix3 = np.array([1,2,3]*10).reshape(10,3).transpose()
# print(matrix3)
#
# file_name = './dataset/EUR-Lex/inv_prop.txt'
#
# file = open(file_name, 'r')
# lines = file.readlines()
# value_list = []
# for line in lines:
#     string = line.strip()
#     value_list.append(float(string))
#
# print(value_list)
#
# file.close()

from fastXML import *
from dataStructure import *
import time
import sys
import matplotlib.pyplot as plt
import numpy as np
import math

# a = 1
# b = 1
# calculate_ndcg(a,b)
#
# a = sqrt(4)
# print(a)
# set1 = set([1,2,3])
# set2 = set([1])
# set3 = set1 - set2
# print(set3)

# x=np.linspace(-4,4,200)
# f1=np.power(10,x) #生成以10为底的指数函数
# f2=np.power(np.e,x) #生成以e为底的指数函数
# f3=np.power(2,x) #生成以2为底的指数函数
# plt.plot(x,f1,'r',x,f2,'b',x,f3,'g',linewidth=2)
# plt.axis([-4,4,-0.5,8])
# plt.text(1,7.5,r'$10^x$',fontsize=16)
# plt.text(2.2,7.5,r'$e^x$',fontsize=16)
# plt.text(3.2,7.5,r'$2^x$',fontsize=16)
# plt.title('A simple example',fontsize=16)
# plt.show()

# list1 = [1,2,3,4,5]
# array1 = np.array(list1)
# index = [0,3,1]
# array2 = array1[index]
# print(array2)

test = exp(-1)
print(test)

array1 = np.array([1,2,4])
array2 = np.log2(array1)
print(array2)