from fastXML import *
from dataStructure import *
import time
import sys
# file = open('./Bibtex/Bibtex_data.txt', 'r')
# line = file.readline()
# print(line)

# train_feature = SparseMatrix('train_feature.txt')  # read the feature data
# train_label = SparseMatrix('train_label.txt')      # read the label data
# test_feature = SparseMatrix('test_feature.txt')
# test_label = SparseMatrix('test_feature.txt')

# train_feature = SparseMatrix('./dataset/EUR-Lex/trn_X_Xf.txt')  # read the feature data
# train_label = SparseMatrix('./dataset/EUR-Lex/trn_X_Y.txt')      # read the label data
# test_feature = SparseMatrix('./dataset/EUR-Lex/tst_X_Xf.txt')
# truth_label = SparseMatrix('./dataset/EUR-Lex/tst_X_Y.txt')
num = sys.argv[1]
train_feature_matrix = SparseMatrix('./Data/Delicious/' + num + '/Delicious_train_feature.txt').matrix  # read the feature data
train_label_matrix = SparseMatrix('./Data/Delicious/' + num + '/Delicious_train_label.txt').matrix      # read the label data
test_feature_matrix = SparseMatrix('./Data/Delicious/' + num + '/Delicious_test_feature.txt').matrix
truth_label_matrix = SparseMatrix('./Data/Delicious/' + num + '/Delicious_test_label.txt').matrix
# time_begin = time.time()
# for i in range(1):
#     train_feature_matrix = SparseMatrix('./dataset/Bibtex/Bibtex_train_feature.txt').matrix  # read the feature data
# print('the cost of time:',time.time()-time_begin)

# data_set_name = 'Bibtex'
data_set_name = 'Delicious'
param = 50  # number of trees
# print(feature_data.matrix)
#
# instances = [0,1,2,3,4,5,6,7,8,9,10,11]
# # instances = [0,10]
#
# # wt_nDCG = []  # the weight of nDCG
# (label_num_row,label_num_col) = label_data.matrix.shape
# wt_nDCG = 1 / numpy.log2(2 + numpy.array(range(label_num_row)))
#
# fastXML.split_node(instances,feature_data.matrix,label_data.matrix,wt_nDCG)

# fastXML.train_tree(feature_data.matrix, label_data.matrix)

time_begin = time.time()
train_trees(train_feature_matrix, train_label_matrix, data_set_name, param)
print('the time of training:', time.time() - time_begin)
#
time_begin = time.time()
test_result = test_trees(test_feature_matrix, truth_label_matrix, data_set_name, param)
print('the time of test:', time.time() - time_begin)
#
time_begin = time.time()
eval_result(test_result, truth_label_matrix, data_set_name)
print('the time of evaluation:', time.time() - time_begin)
