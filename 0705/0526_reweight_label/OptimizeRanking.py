from math import *
from dataStructure import *
from numpy import argsort

label_data = SparseMatrix('train_label.txt')
# print(label_data.nc)
# print(label_data.nr)
# for list in label_data.size:
#      print(list)
# for list in label_data.data:
#      print(list)
# for element in label_data.matrix.getcol(0).toarray():
#     print(element)

# indices of instances
instances = [0,1,2,3,4,5,6,7,8,9,10,11]

# initial partition indicators
pos_neg = [-1,1,-1,1,-1,1,-1,1,-1,1,-1,1]

#
wt_nDCG = []
norm_coef_nDCG = []

num_col = label_data.num_col
num_row = label_data.num_row
num_nonzero = label_data.num_nonzero
data = label_data.data

ndcg = -1
new_ndcg = 0

# see Eq.() on page ? in the paper
for i in range(0,num_row):
    wt_nDCG.append(1 / log2(1 + i + 1))
# for wt in wt_nDCG:
#      print(wt)

for i in range(0,num_col):
     num = num_nonzero[i]

     temp = 0.0
     for j in range(0,num):
         temp += wt_nDCG[j]

     norm_coef_nDCG.append(1.0 / temp)
# for coefficient in norm_coef_nDCG:
#      print(coefficient)

while True:

  # pos_list = []
  # neg_list = []
  # for i in range(0,num_row):
  #   pos_list.append(0.0)
  #   neg_list.append(0.0)

  pos_array = numpy.zeros((1, num_row))
  neg_array = numpy.zeros((1, num_row))
  # print(pos_array)
  # print(neg_array)
  # print('{}{}'.format('pos_array  = ',type(pos_array)))

  for i in range(0, num_col):
      coefficient = norm_coef_nDCG[i]
      instance_label = label_data.matrix.getcol(i).toarray().transpose()
      if (1 == pos_neg[i]):
          # pos_list += instance_label * coefficient
          pos_array += instance_label * coefficient
      else:
          # neg_list += instance_label * coefficient
          neg_array += instance_label * coefficient

  # pos_list = pos_list.tolist()
  # neg_list = neg_list.tolist()

  # for i in range(0,num_col):
  #   instance_label = data[i]
  #   coefficient = norm_coef_nDCG[i]
  #
  #   for j in range(0,len(instance_label)):
  #       row_index = instance_label[j][0]
  #
  #       if(1 == pos_neg[i]):
  #           pos_list[row_index] += coefficient * instance_label[j][1]
  #       else:
  #           neg_list[row_index] += coefficient * instance_label[j][1]
  #
  # for element in pos_array:
  #     print(element)
  # for element in neg_array:
  #     print(element)

  # pos_ranking = argsort(pos_list)[::-1]
  # pos_ranking = argsort([x * -1 for x in pos_list])
  # pos_rank_indices = argsort([x * -1 for x in pos_array])
  pos_rank_indices = argsort(-pos_array).flatten().tolist()
  # for i in pos_rank_indices:
  #     print(i)
  # neg_ranking = argsort([x * -1 for x in neg_list])
  neg_rank_indices = argsort(-neg_array).flatten().tolist()
  # for i in neg_rank_indices:
  #     print(i)
  #
  # print(type(pos_rank_indices))

  # pos_list.sort(reverse=True)
  # neg_list.sort(reverse=True)
  # for ranking in pos_list:
  #     print(ranking)
  # for ranking in neg_list:
  #     print(ranking)


  # pos_list = pos_array.tolist()
  # pos_list.sort(reverse=True)
  # print(pos_array)
  pos_desc_list = sorted(pos_array.flatten(), reverse=True)
  neg_desc_list = sorted(neg_array.flatten(), reverse=True)
  # print(type(pos_desc_list))
  # print(pos_desc_list)
  # print(pos_array)
  # print(len(pos_desc_list))
  # for element in pos_desc_list:
  #     print(element)
  # for element in neg_desc_list:
  #     print(element)

  new_ndcg = 0.0
  for i in range(0,num_row):
    new_ndcg += wt_nDCG[i] * (pos_desc_list[i] + neg_desc_list[i])
  new_ndcg /= num_col
  if abs(new_ndcg - ndcg) < 1e-6 : break
  else: ndcg = new_ndcg

  # print('{}{}'.format('new_ndcg = ',new_ndcg))



  for i in range(0,num_col):
    # instance_label = label_data.matrix.getcol(i).toarray().transpose()
    # instance_label = label_data.matrix.getcol(i).toarray().ravel() # [1.0, 1.0, 1.0, 0.0, 0.0]
    # print(instance_label)
    # print(type(instance_label))
    instance_label = label_data.matrix.getcol(i)
    # print(instance_label)
    # print(type(instance_label))
    # print(instance_label.getnnz())
    (row_array,col_array) =  instance_label.nonzero()
    value_list = instance_label.data.tolist()
    # print(row_array)
    # print(type(row_array))
    # print(type(instance_label.data))
    # print(type(row.tolist()))
    # print(value_list)
    # print(type(value_list))
    # for element in value_list:
    #     print(element)

    diff = 0.0
    for j in range(0, len(value_list)):
        value = value_list[j]
        row_index = row_array[j]
        # neg_index = numpy.argwhere(pos_rank_indices.flat.tolist() == row_index)
        # print(neg_rank_indices.flatten().tolist())
        neg_index = neg_rank_indices.index(row_index)
        pos_index = pos_rank_indices.index(row_index)

        # pos_index = numpy.argwhere(neg_rank_indices == row_index)
        diff += -1 * (1 / log2(1 + neg_index + 1) - 1 / log2(1 + pos_index + 1)) * value * norm_coef_nDCG[i]

    print(diff)
    if(diff < 0): pos_neg[i] = -1
    else: pos_neg[i] = 1

  for i in pos_neg:
    print(i)