#import  DataStructure
from dataStructure import *

# print ("hello world")
# class SparseMatrix:
#     def __init__(self, file_name = ''):
#         self.nc = 0
#         self.nr = 0
#         self.size = []
#         self.data = []
#
#         file = open(file_name, 'r')
#         first_line = file.readline().strip().split(" ")
#         self.nc = int(first_line[0])
#         self.nr = int(first_line[1])
#
#         lines = file.readlines()
#         for line in lines:
#             pairs = line.strip().split(" ")
#
#             pair_dict_list = []
#             for pair in pairs:
#                 pair_string = pair.split(":")
#                 pair_dict = {int(pair_string[0]): float(pair_string[1])}
#                 pair_dict_list.append(pair_dict)
#                 # temp_list = pair_list
#                 # print(pair_list)
#             self.size.append(len(pair_dict_list))
#             self.data.append(pair_dict_list)

c1 = SparseMatrix('train_label.txt')
for element in c1.matrix.toarray():
    print(element)
print(c1.num_col)
print(c1.num_row)
for list in c1.num_nonzero:
    print(list)
# for list in c1.data:
#     print(list)

instances = [0,1,2,3,4,5]
depth = 1
max_leaf = 10

node1 = Node(instances,depth,max_leaf)
node1.WriteNode('nodefile.pkl')

node2 = Node.ReadNode('nodefile.pkl')
node2.is_leaf = False
node2.pos_child = 1
node2.pos_child = 1
node2.depth = 2
node2.instances = [6,7,8,9,10,11]

print(node2.is_leaf)
print(node2.pos_child)
print(node2.neg_child)
print(node2.depth)
print(node2.instances)
print(node2.w)
print(node2.leaf_ranking)

tree = Tree([node1,node2])
tree.WriteTree('treeFile.pkl')

tree = Tree.ReadTree('treeFile.pkl')
for node in tree.nodes:
    print(node.is_leaf)
    print(node.pos_child)
    print(node.neg_child)
    print(node.depth)
    print(node.instances)
    print(node.w)
    print(node.leaf_ranking)




# nc = 0
# nr = 0
# file = open('train_feature.txt','r')
# first_line = file.readline().strip().split(" ")
# nc = int(first_line[0])
# nr = int(first_line[1])
#
# data = []
# #pair_list = []
# lines = file.readlines()
# for line in lines:
#     pairs = line.strip().split(" ")
#
#     pair_dict_list = []
#     for pair in pairs:
#         pair_string = pair.split(":")
#         pair_dict = {int(pair_string[0]): float(pair_string[1])}
#         pair_dict_list.append(pair_dict)
#         #temp_list = pair_list
#         #print(pair_list)
#
#     data.append(pair_dict_list)
#
# for list in data:
#     print(list)

#print(data_lines)

# print(first_line)
# print(nc)
# print(nr)

# l = open('train_feature.txt').readlines()
# for line in l:
#     print(line, end = '')